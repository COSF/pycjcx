# -*- coding: UTF-8 -*-

import os
import base64
import sys
import re
import requests
from io import BytesIO
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash,make_response,current_app


reload(sys)
sys.setdefaultencoding('utf-8')

app = Flask(__name__)
app.secret_key = 'A0Zr9R~^27*(2!X/,?RT'

app.config.update(dict(
    loginUrl = "http://172.16.10.12//jwweb/_data/home_login.aspx",
    codeUrl = "http://172.16.10.12//jwweb/sys/ValidateCode.aspx",
    sourcesUrl = "http://172.16.10.12//jwweb/xscj/Stu_MyScore_rpt.aspx",
    formUrl = "http://172.16.10.12//jwweb/xscj/Stu_MyScore.aspx"
))

def getImageCodeAndCookie():
    codeUrl = current_app.config['codeUrl']
    coostr = None
    if request.cookies.get('ASP.NET_SessionId') != None:
        cookies = {
            'ASP.NET_SessionId': request.cookies.get('ASP.NET_SessionId')
        }
        r = requests.get(codeUrl,cookies=cookies,timeout=300)
        coostr = request.cookies.get('ASP.NET_SessionId')
    else:
        r = requests.get(codeUrl,timeout=300)
        coostr = r.cookies['ASP.NET_SessionId']
    
    img_buffer = BytesIO(r.content)
    imagefile = StringIO()
    content = {
        'cookies': coostr,
        'image': base64.b64encode(img_buffer.getvalue())
    }
    return content

def tryLogin(username,passwprd,vcode):
    loginUrl = current_app.config['loginUrl']
    cookies = {
        'ASP.NET_SessionId': request.cookies.get('ASP.NET_SessionId')
    }
    postData = {
        "Sel_Type":"STU",
        "UserID": username,
        "PassWord": passwprd,
        "cCode" : vcode
    }
    postHeader = {
       "Connection": "keep-alive",
        "Cache-Control": "max-age=0",
        "Origin": "http://172.16.10.12/",
        "Upgrade-Insecure-Requests": "1",
        "Content-Type": "application/x-www-form-urlencoded",
       "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Referer": "http://172.16.10.12//jwweb/_data/home_login.aspx",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,zh-HK;q=0.6"
    }
    r = requests.post(loginUrl,data=postData,cookies = cookies ,headers =postHeader)
    html = unicode(r.content, 'gb2312').encode('utf-8' , 'ignore')
    pattern = re.compile(r'<div align="center"><span id="divLogNote"><font color="White">.*?(帐号或密码不正确.*?|正在加载权限数据.*?|验证码错误.*?).*?</font></span></div>')
    item = re.findall(pattern,html)[0]
    if item == "正在加载权限数据":
        code = 0
    elif item == "帐号或密码不正确":
        code = -1
    elif item == "验证码错误":
        code = -1
    else:
        code = -2
    status = {
        "code": code,
        "message":item
    }
    
    return status

def getSources(sel_xn,sel_xq,SJ,SelXNXQ):
    sourcesUrl = current_app.config['sourcesUrl']
    cookies = {
        'ASP.NET_SessionId': request.cookies.get('ASP.NET_SessionId')
    }
    postData = {
        "SelXNXQ": SelXNXQ,
        "sel_xq": sel_xq,
        "sel_xn": sel_xn,
        "SJ": SJ,
        "zfx_flag": "0",
        "zxf": "0"
    }
    postHeader = {
        "Connection": "keep-alive",
        "Cache-Control": "max-age=0",
        "Origin": "http://172.16.10.12/",
        "Upgrade-Insecure-Requests": "1",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Referer": "http://172.16.10.12//jwweb/xscj/Stu_MyScore.aspx",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,zh-HK;q=0.6"
    }
    r = requests.post(sourcesUrl,data=postData,cookies = cookies ,headers =postHeader)
    html = unicode(r.content, 'gb2312').encode('utf-8' , 'ignore')
    myScores = []
    xq = ''
    name = []
    temp = []
    if SJ == '0':
        pattern = re.compile(r'<td width=3% align=center>(.*?)<br></td><td width=24% align=>(.*?)<br></td><td width=4% align=right>(.*?)<br></td><td width=15% align=left>(.*?)<br></td><td width=9% align=left>(.*?)<br></td><td width=4% align=center>(.*?)<br></td><td width=4% align=center>(.*?)<br></td><td width=5% align=right>(.*?)<br></td><td width=5% align=right><br></td><td width=5% align=right>(.*?)<br></td><td width=5% align=right><br></td><td width=5% align=right>(.*?)<br></td><td width=4% align=center>(.*?)<br></td><td width=8% align=left><br></td>')
        item = pattern.findall(html)
        item.reverse()
        for echoI in item:
            item = {}
            item['xuhao'] = echoI[0]
            course = re.sub(r'\[\d*?\](.*?)',"",echoI[1])
            item['kecheng'] = course
            item['xuefeng'] = echoI[2]
            item['leibie'] = echoI[3]
            item['kechengleibie'] = echoI[4]
            item['kaohe'] = echoI[5]
            item['xiudu'] = echoI[6]
            item['pingshi'] = echoI[7]
            item['mokao'] = echoI[8]
            item['zonghe'] = echoI[9]
            item['fuxiu'] = echoI[10]
            temp.append(item)
            if item['xuhao'] == '1':
                temp.reverse()
                myScores.append(temp)
                temp = []
        pattern = re.compile(r'<td align=left>&nbsp;学年学期：(.*?)</td>')
        xq = pattern.findall(html)
        xq.reverse()
    else:
        pattern = re.compile(r"<td width=16% align=center>(.*?)<br></td><td width=23% align=left>(.*?)<br></td><td width=4% align=right>(.*?)<br></td><td width=14% align=left>(.*?)<br></td><td width=9% align=left>(.*?)<br></td><td width=4% align=center>(.*?)<br></td><td width=4% align=center>(.*?)<br></td><td width=5% align=right>(.*?)<br></td><td width=4%\s?align=right>(.*?)<br></td><td width=4% align=right>(.*?)<br></td><td width=5% align=right>(.*?)<br></td><td width=8% align=left><br></td>")
        item = pattern.findall(html)
        item.reverse()
        for echoI in item:
            item = {}
            item['xq'] = echoI[0]
            course = re.sub(r'\[\d*?\](.*?)',"",echoI[1])
            item['kecheng'] = course
            item['xuefeng'] = echoI[2]
            item['leibie'] = echoI[3] 
            item['kechengleibie'] = echoI[4]
            item['kaohe'] = echoI[5]
            item['xiudu'] = echoI[6]
            item['chengji'] = echoI[7]
            item['qudexuefeng'] = echoI[8]
            item['jidian'] = echoI[9]
            item['xuefengjidian'] = echoI[10]
            temp.append(item)
            if echoI[0] != '':
                temp.reverse()
                myScores.append(temp)
                temp = []
    pattern = re.compile(r'<td align=left>&nbsp;学号：(\d*?)&nbsp;&nbsp; 姓名：(.*?)<table')
    name = pattern.findall(html)
    data = {
        "scores": myScores,
        "xueqi": xq,
        "sj":SJ,
        "name":name
    }
    return data

def getForm():
    formUrl = current_app.config['formUrl']
    cookies = {
        'ASP.NET_SessionId': request.cookies.get('ASP.NET_SessionId')
    }
    postHeader = {
        "Connection": "keep-alive",
        "Cache-Control": "max-age=0",
        "Origin": "http://172.16.10.12/",
        "Upgrade-Insecure-Requests": "1",
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Referer": "http://172.16.10.12//jwweb/xscj/Stu_MyScore.aspx",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,zh-HK;q=0.6"
    }
    r = requests.get(formUrl,cookies = cookies ,headers =postHeader)
    html = unicode(r.content, 'gb2312').encode('utf-8' , 'ignore')
    pattern = re.compile(r"<option value='(.*?)'>(.*?)</option>")
    item = pattern.findall(html)
    xnxq=[]
    for echoI in item:
        item={}
        item['value'] = echoI[0]
        item['text'] = echoI[1]
        xnxq.append(item)
    data = {
        "xnxq": xnxq
    }

    return data

@app.route('/',methods=['GET', 'POST'])
def index():
    if 'logged_in' in session:
        if request.args.get('zfx_flag', '') == "0":
            sel_xn = request.args.get('sel_xn', '')
            sel_xq = request.args.get('sel_xq', '')
            SJ = request.args.get('SJ', '')
            SelXNXQ = request.args.get('SelXNXQ', '')
            data = getSources(sel_xn,sel_xq,SJ,SelXNXQ)
            scores = data['scores']
            xq = data['xueqi']
            sj = data['sj']
            name = data['name']
            return make_response(render_template('home.html',data=scores,xq=xq,sj=sj,name=name))
        
        data = getForm()
        if data['xnxq'] == []:
            return redirect('/login')
        return make_response(render_template('select.html',xnxq=data['xnxq']))

    flash('You are not logged in')

    return redirect('/login')


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    title = "-登录"

    if request.method == 'POST':
        if request.form['username'] == "":
            error = "用户名不能为空！"
        elif request.form['password'] == "":
            error = "密码不能为空！"
        elif request.form['vcode'] == "":
            error = "验证码不能为空！"
        else:
            status = tryLogin(request.form['username'],request.form['password'],request.form['vcode'])
            if status['code'] != 0:
                error = status['message']
            else:
                session['logged_in'] = True
                flash(status['message'])
                return redirect('/')
    
    content = getImageCodeAndCookie()
    img = content['image']
    cookies = content['cookies']
    resp = make_response(render_template('login.html',img=img,error=error,title=title))
    resp.set_cookie('ASP.NET_SessionId',cookies)
    return resp

@app.route('/logout')
def logout():
    resp = make_response()
    resp.set_cookie('ASP.NET_SessionId', '', expires=0)
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect('/')

if __name__ == '__main__':
    app.run()