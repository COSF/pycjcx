# 轻院成绩爬虫

基于 python 和 flask 的成绩查询爬虫

## 依赖
```
python 2+
os
base64
sys
re
requests
io
flask
```
## 使用

```
git clone https://gitlab.com/COSF/pycjcx.git
cd pycjcx
python cjcx.py
```